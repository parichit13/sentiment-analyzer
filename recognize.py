import speech_recognition as sr
import os
import webbrowser
import platform

r = sr.Recognizer()
m = sr.Microphone()

try:
    print("A moment of silence, please...")
    with m as source:
        r.adjust_for_ambient_noise(source)
        print("Set minimum energy threshold to {}".format(r.energy_threshold))
        sys = platform.system()
        if(sys == 'Linux'):
            os.system("clear")
        else:
            os.system("cls")
        while True:
            print("Say something!")
            audio = r.listen(source)
            print("Got it! Now to recognize it...")
            try:
                # recognize speech using Google Speech Recognition
                value = r.recognize_google(audio)
                # we need some special handling here to correctly print unicode characters to standard output
                if str is bytes: # this version of Python uses bytes for strings (Python 2)
                    print(u"You said {}".format(value).encode("utf-8"))
                    f = open("input.txt", "w");
                    f.write(format(value).encode("utf-8"));
                    f.close();
                    # print("Lower version: " + value.lower())
                    # if value.lower() == "open firefox":
                    #     controller.open_new_tab('http://www.google.com')
                else: # this version of Python uses unicode for strings (Python 3+)
                    print("You said {}".format(value))
                print("--------------------------------------------------------")
                print("                     Now Recognizing                    ")
                print("")
                os.system("java -cp \"*\" -mx5g edu.stanford.nlp.sentiment.SentimentPipeline -file input.txt");
                break
            except sr.UnknownValueError:
                print("Oops! Didn't catch that")
            except sr.RequestError as e:
                print("Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e))
except KeyboardInterrupt:
    pass